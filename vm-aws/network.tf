locals {
  security_group_rules = distinct(concat(
    [for item in var.security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")],
    [for item in var.config_security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")]
  ))
}

resource "aws_vpc" "vpc" {
  count                = local.external-network != null ? 0 : 1
  cidr_block           = var.cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "${var.workspace_id}-vpc"
  }
}

resource "aws_internet_gateway" "igw" {
  count  = local.external-network != null ? 0 : 1
  vpc_id = aws_vpc.vpc[0].id

  tags = {
    Name = "${var.workspace_id}-igw"
  }
}

resource "aws_subnet" "subnet" {
  count             = local.external-network != null ? 0 : 1
  vpc_id            = aws_vpc.vpc[0].id
  cidr_block        = var.cidr
  availability_zone = var.availability_zone

  tags = {
    Name = "${var.workspace_id}-subnet"
  }
}

resource "aws_route_table" "router" {
  count  = local.external-network != null ? 0 : 1
  vpc_id = aws_vpc.vpc[0].id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw[0].id
  }

  tags = {
    Name = "${var.workspace_id}-router"
  }
}

resource "aws_route_table_association" "table_association" {
  count          = local.external-network != null ? 0 : 1
  subnet_id      = aws_subnet.subnet[0].id
  route_table_id = aws_route_table.router[0].id
}

resource "aws_security_group" "secgroup" {
  name   = "${var.workspace_id}-secgroup"
  vpc_id = local.external-network != null ? local.external-network.vpc_id.value : aws_vpc.vpc[0].id

  tags = {
    Name = "${var.workspace_id}-secgroup"
  }
}

resource "aws_security_group_rule" "secgroup_rule" {
  count             = length(var.security_group_rules)
  type              = split(" ", local.security_group_rules[count.index])[0] == "in" ? "ingress" : "egress"
  protocol          = split(" ", local.security_group_rules[count.index])[1]
  from_port         = split(" ", local.security_group_rules[count.index])[2]
  to_port           = split(" ", local.security_group_rules[count.index])[3]
  cidr_blocks       = [split(" ", local.security_group_rules[count.index])[4]]
  security_group_id = aws_security_group.secgroup.id
}

resource "aws_eip" "ip-aws" {
  count  = local.external-floating-ip != null ? 0 : 1
  domain = "vpc"
  tags = {
    Name = var.instance_name
  }
}

resource "aws_eip_association" "internal-floatip" {
  count       = local.external-floating-ip != null ? 0 : 1
  public_ip   = aws_eip.ip-aws[0].public_ip
  instance_id = aws_instance.vm-aws.id
  depends_on  = [aws_internet_gateway.igw]
}

resource "aws_eip_association" "external-floatip" {
  count       = local.external-floating-ip != null ? 1 : 0
  public_ip   = local.external-floating-ip.address.value
  instance_id = aws_instance.vm-aws.id
  depends_on  = [aws_internet_gateway.igw]
}
