output "vm_id" {
  value = aws_instance.vm-aws.id
}

output "id" {
  value = aws_instance.vm-aws.id
}

output "ip" {
  value = local.external-floating-ip != null ? local.external-floating-ip.address.value : aws_eip_association.internal-floatip[0].public_ip
}

output "local_ip" {
  value = local.external-network != null ? aws_instance.vm-aws.private_ip : null
}

output "instance_password" {
  value = (aws_instance.vm-aws.password_data != "" && tls_private_key.private_key.private_key_pem != ""
    ? rsadecrypt(aws_instance.vm-aws.password_data, tls_private_key.private_key.private_key_pem)
  : "")
  sensitive = true
}

output "min_os_disk_size" {
  value = var.min_os_disk_size
}

output "instance_user" {
  value = var.instance_user
}

output "private_key" {
  value     = tls_private_key.private_key.private_key_pem
  sensitive = true
}

output "public_key" {
  value = tls_private_key.private_key.public_key_openssh
}

output "flavor_name" {
  value = var.instance_type
}

# Mark support for updating vm NSGs
output "terraform_script_version" {
  value = 3.0
}
