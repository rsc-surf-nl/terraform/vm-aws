locals {
  tag_map = {
    application_type   = var.application_type
    cloud_type         = var.cloud_type
    co_id              = var.co_id
    resource_type      = var.resource_type
    subscription       = var.subscription
    subscription_group = var.subscription_group
    wallet_id          = var.wallet_id
    workspace_id       = var.workspace_id
  }
  external-volumes     = tolist(jsondecode(var.external_volumes))
  external-floating-ip = length(tolist(jsondecode(var.external_floating_ips))) != 0 ? tolist(jsondecode(var.external_floating_ips))[0] : null
  external-network     = length(tolist(jsondecode(var.external_networks))) != 0 ? tolist(jsondecode(var.external_networks))[0] : null
  dns_ipv4             = local.external-floating-ip == null ? aws_eip_association.internal-floatip[0].public_ip : local.external-floating-ip.address.value
}

data "aws_ami" "image" {
  most_recent = true
  owners      = [var.image_owner_account]

  filter {
    name   = "name"
    values = [var.image_name]
  }
}

resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {
  key_name   = "${var.workspace_id}-keypair"
  public_key = tls_private_key.private_key.public_key_openssh
}

resource "aws_instance" "vm-aws" {
  depends_on             = [aws_vpc.vpc, aws_subnet.subnet, aws_security_group.secgroup] // we need depends_on because terraform ifs are stupid
  ami                    = data.aws_ami.image.id
  instance_type          = var.instance_type
  key_name               = aws_key_pair.generated_key.key_name
  vpc_security_group_ids = [aws_security_group.secgroup.id]
  subnet_id              = local.external-network != null ? local.external-network.subnet_id.value : aws_subnet.subnet[0].id
  get_password_data      = var.get_password
  availability_zone      = var.availability_zone
  iam_instance_profile   = var.iam_instance_profile
  tags = {
    Name         = var.instance_name
    instance_tag = var.instance_tag
  }
  root_block_device {
    delete_on_termination = true
    volume_size           = max(var.boot_volume_size, var.min_os_disk_size)
  }
  volume_tags = merge(
    local.tag_map,
    {
      Name         = var.instance_name
      instance_tag = var.instance_tag
    }
  )
}

resource "aws_volume_attachment" "external-volumes-attach" {
  count        = length(local.external-volumes)
  volume_id    = local.external-volumes[count.index].id.value
  instance_id  = aws_instance.vm-aws.id
  force_detach = true
  device_name  = var.device_names[count.index]
}

resource "aws_s3_bucket" "workspace_s3_bucket" {
  count         = var.s3_bucket_prefix == "" ? 0 : 1
  bucket        = "${var.s3_bucket_prefix}-${var.workspace_id}"
  force_destroy = true

  tags = {
    Name         = var.instance_name
    instance_tag = var.instance_tag
  }
}
