variable "image_owner_account" {}
variable "image_name" {}
variable "instance_type" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_session_token" {
  type        = string
  description = "Optional; the AWS session token"
  default     = ""
}
variable "aws_region" {}
variable "instance_name" {
  default = "rsc-instance"
}
variable "instance_user" {}
variable "instance_tag" {}
variable "availability_zone" {}
variable "get_password" {
  default = false
}

variable "boot_volume_size" {
  type    = number
  default = 15
}
variable "security_group_rules" {
  type = list(string)
}

variable "config_security_group_rules" {
  type    = list(string)
  default = []
}
variable "cidr" {}

variable "min_os_disk_size" {
  type    = number
  default = 20
}
variable "assume_role_arn" {
  default = ""
}
variable "assume_role_session_name" {
  default = ""
}
variable "iam_instance_profile" {
  default = ""
}
variable "device_names" {
  type    = list(string)
  default = ["/dev/xvdf", "/dev/xvdg", "/dev/xvdh", "/dev/xvdi", "/dev/xvdj", "/dev/xvdk", "/dev/xvdl", "/dev/xvdm", "/dev/xvdn", "/dev/xvdo", "/dev/xvdp"]
}
variable "external_volumes" {
  default = "[]"
}
variable "external_floating_ips" {
  default = "[]"
}
variable "external_networks" {
  default = "[]"
}

variable "s3_bucket_prefix" {
  type        = string
  description = "Will create a S3 resources"
  default     = ""
}

# SRC automatic variables are added explicitly to be self-contained
variable "application_type" {
  type        = string
  description = "The application type (eg. 'Compute', 'Storage' etc.)"
  default     = "Compute"
}
variable "cloud_type" {
  type        = string
  description = "Cloud type (eg. 'AWS', 'Openstack', 'Azure' etc.)"
  default     = "AWS"
}
variable "co_id" {
  type        = string
  description = "The ID of the associated CO"
  default     = "unset"
}
variable "resource_type" {
  type        = string
  description = "The resource type (eg. 'VM', 'Storage-Volume' etc.)"
  default     = "VM"
}
variable "subscription" {
  type        = string
  description = "The (SRC) subscription under which this resources is created"
  default     = "unset"
}
variable "subscription_group" {
  type        = string
  description = "The subscription group to which the `subscription` belongs"
  default     = "unset"
}
variable "wallet_id" {
  type        = string
  description = "The ID of the associated wallet"
  default     = "unset"
}
variable "workspace_fqdn" {
  type        = string
  description = "The FQDN assigned to this workspace"
  default     = "example.surf-hosted.nl"
}
variable "workspace_id" {
  type        = string
  description = "The ID of this workspace"
  default     = "unset"
}
