terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      # Changing the version here won't help in production, as the version is being
      # pinned by the workspace-deployer microservice as well.
      version = "~> 5.56.1"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.5"
    }
  }
  required_version = ">= 1.6"
}
